import json
import os
import sys

import tkinter
import tkinter.filedialog
import tkinter.messagebox
import tkinter.simpledialog


class ProgressBar(tkinter.Frame):

    def __init__(self):
        super().__init__()
        self.variable = tkinter.StringVar()
        self.label = tkinter.Label(
            self,
            bd=1,
            relief=tkinter.SUNKEN,
            anchor=tkinter.W,
            textvariable=self.variable
        )
        self.label.pack(fill=tkinter.X)
        self.pack(side=tkinter.BOTTOM, fill=tkinter.X)


class Tab:

    def __init__(self, window, menu, name, on_click_handler):
        self.header = tkinter.Frame(menu)
        self.header_button = tkinter.Button(
            master=self.header,
            text=name,
            command=lambda x=self: on_click_handler(self),
            relief=tkinter.RAISED
        )
        self.header_button.pack()
        self.text = tkinter.Text(
            master=window,
            undo=True,
            exportselection=True,
            bg='black',
            fg='white',
            insertbackground="white",
            bd=0
        )
        self.rename_entry = tkinter.Entry(master=self.header)
        self.rename_entry.bind('<Return>', self.apply_rename)
        self.rename_entry.bind('<FocusOut>', self.apply_rename)
        self.rename_entry.bind('<Escape>', self.discard_rename)

    def enable_rename_mode(self):
        name = self.header_button['text']
        self.header_button.pack_forget()
        self.rename_entry.pack()
        self.rename_entry.focus_set()
        self.rename_entry.delete(0, tkinter.END)
        self.rename_entry.insert(0, name)
        self.rename_entry.select_range(0, tkinter.END)
        self.rename_entry.icursor(tkinter.END)

    def apply_rename(self, _=None):
        new_name = self.rename_entry.get()
        self.rename_entry.pack_forget()
        self.header_button.pack()
        self.header_button.config(text=new_name)

    def discard_rename(self, _=None):
        self.rename_entry.pack_forget()
        self.header_button.pack()


class GUI:

    def __init__(self, initial_file_to_open=None):
        self.win = tkinter.Tk()
        self.tabs = []
        self.active_tab = None
        self.active_file = None
        self.unmodified_data = None
        self.is_ctrl_pressed = False
        self.handlers = {
            78: self.new_note_handler,
            79: self.open_note_handler,
            83: self.save_note_handler,
            188: self.move_active_tab_left_handler,
            190: self.move_active_tab_right_handler
        }

        self.panel = self.generate_initial_panel()
        self.new_topic_button = tkinter.Button(
            master=self.panel,
            text='+',
            command=self.new_topic_button_handler
        )
        self.new_topic_button.pack(side=tkinter.LEFT)
        self.remove_topic_button = tkinter.Button(
            master=self.panel,
            text='-',
            command=self.remove_topic_button_handler
        )
        self.remove_topic_button.pack(side=tkinter.RIGHT)

        if initial_file_to_open:
            self.open_note(initial_file_to_open)
        else:
            self.new_note()

        self.progress_bar = ProgressBar()

    def generate_initial_panel(self):
        panel = tkinter.Frame(master=self.win)
        panel.pack(side=tkinter.TOP, expand=False, fill=tkinter.X)

        note_menu = tkinter.Menu(master=self.win, tearoff=0)
        note_menu.add_command(label='New', command=self.new_note_handler, accelerator="Ctrl N")
        note_menu.add_command(label='Open', command=self.open_note_handler, accelerator="Ctrl P")
        note_menu.add_command(label='Save', command=self.save_note_handler, accelerator="Ctrl S")

        note_menu_button = tkinter.Button(
            master=panel,
            text="Note",
            command=lambda: note_menu.post(panel.winfo_rootx(), panel.winfo_rooty() + panel.winfo_height())
        )
        note_menu_button.pack(side=tkinter.LEFT)

        tkinter\
            .Frame(master=panel, width=10, bg="black")\
            .pack(side=tkinter.LEFT, expand=False, fill=tkinter.Y)

        return panel

    def on_tab_switch(self, tab):
        if tab == self.active_tab:
            tab.enable_rename_mode()
            return

        if self.active_tab is not None:
            self.active_tab.header_button.config(relief=tkinter.RAISED)
            self.active_tab.text.pack_forget()

        tab.header_button.config(relief=tkinter.FLAT)
        tab.text.pack(side=tkinter.TOP, expand=True, fill=tkinter.BOTH)

        self.active_tab = tab

    def save_note(self, file_path):
        self.progress_bar.variable.set('Saving...')
        new_data = self.get_data_in_json_string()

        with open(file_path, 'w', encoding='utf-8') as f:
            f.write(new_data)

        self.unmodified_data = new_data
        self.progress_bar.variable.set('Saved')
        self.win.after(3000, lambda x=self.progress_bar.variable: x.set(''))

    def open_note(self, file_path):
        with open(file_path, 'r', encoding='utf-8') as f:
            records = json.load(f)

        if not records:
            tkinter.messagebox.showerror('Open error', 'File is not valid .ntr file. Creating new one.')
            self.new_note()
            return

        self.new_topic_button.pack_forget()

        for record in records:
            self.tabs.append(Tab(self.win, self.panel, record['name'], self.on_tab_switch))
            self.tabs[-1].text.insert(1.0, record['text'])
            self.tabs[-1].header.pack(side=tkinter.LEFT)

        self.new_topic_button.pack(side=tkinter.LEFT)

        self.on_tab_switch(self.tabs[0])
        self.active_file = file_path
        self.unmodified_data = self.get_data_in_json_string()

    def new_note(self):
        self.active_file = None
        self.active_tab = None

        self.new_topic()
        self.unmodified_data = self.get_data_in_json_string()

    def new_topic(self):
        self.new_topic_button.pack_forget()

        self.tabs.append(Tab(self.win, self.panel, 'topic', self.on_tab_switch))
        self.tabs[-1].header.pack(side=tkinter.LEFT)

        self.new_topic_button.pack(side=tkinter.LEFT)

        self.on_tab_switch(self.tabs[-1])

    def remove_current_topic(self):
        self.active_tab.header.destroy()
        self.active_tab.text.destroy()
        index = self.tabs.index(self.active_tab)
        self.tabs.remove(self.active_tab)
        self.active_tab = None

        if len(self.tabs) == 0:
            self.new_topic()
            return

        self.on_tab_switch(self.tabs[max(0, index-1)])

    def clear_tabs(self):
        for tab in self.tabs:
            tab.header.destroy()
            tab.text.destroy()

        self.tabs = []
        self.active_tab = None

    def try_define_active_file(self):
        if self.active_file is None:
            self.active_file = tkinter.filedialog.asksaveasfilename(initialfile='notes.ntr')
            if not self.active_file:
                self.active_file = None
                return False

        return True

    def get_data_in_json_string(self):
        records = [{
            'name': x.header_button['text'],
            'text': x.text.get(1.0, tkinter.END)[:-1]
        } for x in self.tabs]
        return json.dumps(records, ensure_ascii=False)

    @staticmethod
    def ask_for_save():
        return tkinter.messagebox.askyesnocancel('Save', 'Save current modifications?')

    @property
    def is_data_modified(self):
        return self.get_data_in_json_string() != self.unmodified_data

    # region Handlers

    def open_note_handler(self):
        to_open = tkinter.filedialog.askopenfilename(filetypes=(('Noter files', '*.ntr'),))
        if not to_open:
            return

        if self.is_data_modified:
            need_save = self.ask_for_save()
            if need_save is None:
                return
            elif need_save:
                if not self.try_define_active_file():
                    return
                self.save_note(self.active_file)

        self.clear_tabs()
        self.open_note(to_open)

    def new_note_handler(self):
        if self.is_data_modified:
            need_save = self.ask_for_save()
            if need_save is None:
                return
            elif need_save:
                if not self.try_define_active_file():
                    return
                self.save_note(self.active_file)

        self.clear_tabs()
        self.new_note()

    def save_note_handler(self):
        if not self.try_define_active_file():
            return

        self.save_note(self.active_file)

    def new_topic_button_handler(self):
        self.new_topic()
        self.tabs[-1].enable_rename_mode()

    def remove_topic_button_handler(self):
        self.remove_current_topic()

    def app_close_handler(self):
        if self.is_data_modified:
            need_save = self.ask_for_save()
            if need_save is None:
                return
            elif need_save:
                if not self.try_define_active_file():
                    return
                self.save_note(self.active_file)

        self.win.destroy()

    def move_active_tab_left_handler(self):
        current_idx = self.tabs.index(self.active_tab)
        if current_idx == 0:
            return
        for tab_idx in range(current_idx - 1, len(self.tabs)):
            self.tabs[tab_idx].header.pack_forget()
        self.new_topic_button.pack_forget()
        self.tabs.remove(self.active_tab)
        self.tabs.insert(current_idx - 1, self.active_tab)
        for tab_idx in range(current_idx - 1, len(self.tabs)):
            self.tabs[tab_idx].header.pack(side=tkinter.LEFT)
        self.new_topic_button.pack(side=tkinter.LEFT)

    def move_active_tab_right_handler(self):
        current_idx = self.tabs.index(self.active_tab)
        if current_idx == len(self.tabs) - 1:
            return
        for tab_idx in range(current_idx, len(self.tabs)):
            self.tabs[tab_idx].header.pack_forget()
        self.new_topic_button.pack_forget()
        self.tabs.remove(self.active_tab)
        self.tabs.insert(current_idx + 1, self.active_tab)
        for tab_idx in range(current_idx, len(self.tabs)):
            self.tabs[tab_idx].header.pack(side=tkinter.LEFT)
        self.new_topic_button.pack(side=tkinter.LEFT)

    # endregion

    def on_key_pressed(self, e):
        if e.keysym == 'Control_L':
            self.is_ctrl_pressed = True
        elif self.is_ctrl_pressed and e.keycode in self.handlers:
            self.handlers[e.keycode]()

    def on_key_released(self, e):
        if e.keysym == 'Control_L':
            self.is_ctrl_pressed = False

    def start(self):
        self.win.bind('<KeyPress>', self.on_key_pressed)
        self.win.bind('<KeyRelease>', self.on_key_released)
        # self.win.bind('<Control-plus>', self.new_topic_handler)
        # self.win.bind('<Control-minus>', self.remove_current_topic_handler)
        # self.win.bind('<Control-asterisk>', self.rename_current_topic_handler)
        self.win.protocol("WM_DELETE_WINDOW", self.app_close_handler)
        self.win.mainloop()


if __name__ == '__main__':
    file_to_open = None
    if len(sys.argv) > 1:
        file_to_open = sys.argv[1]
    else:
        default_file = os.path.join(os.path.split(sys.argv[0])[0], 'notes.ntr')
        if os.path.exists(default_file):
            file_to_open = default_file

    gui = GUI(file_to_open)
    gui.start()
